// AppNikolaï.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>

int main()
{
    int a = 1;
    int b = 5;
    double f = 3.14;
    char c = 'C';
    char d;
    std::cout << "a: valeur = " << a << ", adresse= " << std::hex << &a << std::endl;
    std::cout << "b: valeur = " << b << ", adresse= " << std::hex << &b << std::endl;
    std::cout << "f: valeur = " << f << ", adresse = " << std::hex << &f << std::endl;
    std::cout << "d: valeur = " << c << ", adresse= " << std::hex << &c << std::endl;
    std::cout << "d: adresse = " << std::hex << &d << std::endl;
    d = 'D';
    std::cout << "d: valeur = " << d << std::hex << &d << std::endl;

    std::swap(a, b);
    std::cout << "Après ";
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
